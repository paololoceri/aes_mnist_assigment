#ifndef TYPES_H
#define TYPES_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>


#define __OK__ 1

#define STRLEN 200

            typedef short int DATA;

            #define FIXED2FLOAT(a, qf) (((float) (a)) / (1<<qf))
            #define FLOAT2FIXED(a, qf) ((short int) round((a) * (1<<qf)))
            #define _MAX_ (1 << (sizeof(DATA)*8-1))-1
            #define _MIN_ -(_MAX_+1)
       


typedef int RET;
typedef const int SIZE;
typedef int VARSIZE;
typedef const char* NAME;
typedef char VARNAME[STRLEN];
typedef int ID;
typedef void* USER_DATA;



#ifdef __cplusplus
}
#endif

#endif
