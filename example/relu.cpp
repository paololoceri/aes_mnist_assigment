#include "relu.h"


RELU relu_create(void) {
    RELU r = (RELU) calloc(1, sizeof(struct Relu));
    return r;
}

RET relu_forward_wrap(RELU r) {

	return relu_forward(r->input, r->output, r->size, r->alpha, r->qf);
}


static inline RET relu_forward_sw(DATA* input, DATA* output, SIZE size[3], DATA alpha, int qf) {
	int i = 0;
	SIZE memsize = size[0]*size[1]*size[2];

	for(i = 0; i < memsize; i++) {
		DATA v = input[i];
		v = v > 0 ? v : (v*alpha)>>qf;

		output[i] = v;
	}
	return __OK__;
}



RET relu_forward(DATA* input, DATA* output, SIZE size[3], DATA alpha, int qf) {

	relu_forward_sw(input, output, size, alpha, qf);


	return __OK__;
}


