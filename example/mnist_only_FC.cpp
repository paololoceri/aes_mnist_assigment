
//This code is auto_generated.//

//This file must be embedded in the host application that will provide the data to be processed.



/* ################## Net description ###################
Source file: /opt/data/experiments/prj_5fd9e2cf7ead0f9e0124574d/alg_5fd9e2d47574d329a0b2f8ea/onnx_storage/onnx_model.onnx

Layer                   IF    ih    iw    OF    oh    ow
Gemm0                  784     1     1    64     1     1
Relu0                   64     1     1    64     1     1
Gemm1                   64     1     1    32     1     1
Relu1                   32     1     1    32     1     1
Gemm2                   32     1     1    10     1     1
#########################################################*/

#include "mnist_only_FC.h" 



// global variables
DATA* wPointer;
DATA* aPointer;

LINEAR Gemm0_param;

RELU Relu0_param; 
LINEAR Gemm1_param;

RELU Relu1_param; 
LINEAR Gemm2_param;



void getInputSize(VARSIZE* inSz)
{ // CHW
        inSz[0]=1;
        inSz[1]=28;
        inSz[2]=28;
  }



void getOutputSize(VARSIZE* output_size)
{ 
        *output_size=10;
  }

void cnnMainInit(VARNAME load_data_dir)
{        
VARNAME filename;


// #########################################
//        FULLY CONNECTED LAYER init *
//##########################################

Gemm0_param = linear_create();
Gemm0_param->in_s = 784;
Gemm0_param->out_s = 64;
Gemm0_param->in_s = 784;
 Gemm0_param->qf = 8;


// ###############################################################
// #######           RELU/LEAKY_RELU LAYER init       #############
// #############################################################



 Relu0_param = relu_create();
Relu0_param->size[0] = 64;
Relu0_param->size[1] = 1;
Relu0_param->size[2] = 1;
Relu0_param->alpha    = 0;
Relu0_param->qf    = 8;


// #########################################
//        FULLY CONNECTED LAYER init *
//##########################################

Gemm1_param = linear_create();
Gemm1_param->in_s = 64;
Gemm1_param->out_s = 32;
Gemm1_param->in_s = 64;
 Gemm1_param->qf = 8;


// ###############################################################
// #######           RELU/LEAKY_RELU LAYER init       #############
// #############################################################



 Relu1_param = relu_create();
Relu1_param->size[0] = 32;
Relu1_param->size[1] = 1;
Relu1_param->size[2] = 1;
Relu1_param->alpha    = 0;
Relu1_param->qf    = 8;


// #########################################
//        FULLY CONNECTED LAYER init *
//##########################################

Gemm2_param = linear_create();
Gemm2_param->in_s = 32;
Gemm2_param->out_s = 10;
Gemm2_param->in_s = 32;
 Gemm2_param->qf = 8;


// ##################################################################
// ######################## WEIGHTS LOADING #######################
// ################################################################

//	Only convolutional layers and FC or GEMM layers have weights and biases
//	Convolutional layers will be accelerated and their weights must be stored in the DDR ram chunck accessible by the soc. 
//	The FC and GEMM weights and biases may be stored in any place 

Gemm0_param->weights = (DATA*)malloc(50176 * sizeof(DATA));
sprintf(filename, "%s/Gemm0_weights.bin", load_data_dir);
load_fixed(filename, 50176, Gemm0_param->weights);
Gemm0_param->bias = (DATA*)malloc(64 * sizeof(DATA));
sprintf(filename, "%s/Gemm0_biases.bin", load_data_dir);
load_fixed(filename,64,Gemm0_param->bias);

Gemm1_param->weights = (DATA*)malloc(2048 * sizeof(DATA));
sprintf(filename, "%s/Gemm1_weights.bin", load_data_dir);
load_fixed(filename, 2048, Gemm1_param->weights);
Gemm1_param->bias = (DATA*)malloc(32 * sizeof(DATA));
sprintf(filename, "%s/Gemm1_biases.bin", load_data_dir);
load_fixed(filename,32,Gemm1_param->bias);

Gemm2_param->weights = (DATA*)malloc(320 * sizeof(DATA));
sprintf(filename, "%s/Gemm2_weights.bin", load_data_dir);
load_fixed(filename, 320, Gemm2_param->weights);
Gemm2_param->bias = (DATA*)malloc(10 * sizeof(DATA));
sprintf(filename, "%s/Gemm2_biases.bin", load_data_dir);
load_fixed(filename,10,Gemm2_param->bias);



// ########################################################
// ######################## MEMORY ALLOCATION #######################
// #######################################################

//	Each layer produces an output. By default the output is stored in a dedicated DDR segment accwssible by the accelerator.
//	We assume that the output of a layer will be the input of the netx so we do not need to allocate RAM for the input.
//	By editing this you can customize the memory utilization

Gemm0_param->input = (DATA*)malloc(784*sizeof(DATA));
Gemm0_param->output = (DATA*)malloc(64*sizeof(DATA));
Relu0_param->output = (DATA*)malloc(64*sizeof(DATA));
Gemm1_param->output = (DATA*)malloc(32*sizeof(DATA));
Relu1_param->output = (DATA*)malloc(32*sizeof(DATA));
Gemm2_param->output = (DATA*)malloc(10*sizeof(DATA));



// ########################################################
// ######################## WIRING #######################
// #######################################################

//	Each layer has an input and an output pointer. The connection between two self.net is defined here

Relu0_param->input = Gemm0_param->output;
Gemm1_param->input = Relu0_param->output;
Relu1_param->input = Gemm1_param->output;
Gemm2_param->input = Relu1_param->output;



}

//#########################################################################################################################################
//#########################################################################################################################################


void cnnMain(DATA* image, DATA** results)
{
SIZE input_size[] = {1,28,28}; 
SIZE batch_join_dim = 1 * 28 * 28;
for (int j=0;j<batch_join_dim;j++)
  Gemm0_param->input[j]=image[j];

//printf("\n\n\n\ncccc\n");
//for (int i=0;i<768;i++)
//  printf("0x%x\t",Gemm0_param->input[i]);

//printf("\n\n\n\n");
//exit(0);

//################################################# Linear layer ##########################################################
linear_forward_wrap(Gemm0_param);

//printf("\n\n\n\ncccc\n");
//for (int i=0;i<768;i++)
//  printf("0x%x\t",Gemm0_param->output[i]);

//printf("\n\n\n\n");

//################################################# Relu layer ##########################################################
relu_forward_wrap (Relu0_param);


//################################################# Linear layer ##########################################################
linear_forward_wrap(Gemm1_param);



//################################################# Relu layer ##########################################################
relu_forward_wrap (Relu1_param);


//################################################# Linear layer ##########################################################
linear_forward_wrap(Gemm2_param);



DATA* res = (DATA *) calloc(10, sizeof(DATA));
for(unsigned int i = 0; i < 10; i++){
	res[i] = Gemm2_param->output[i];
//	_dprintf_("[%d] %d \n", i, Gemm2_param->output[i]);
}
*results = res;
}


void getInputImage(VARNAME image_path, DATA* image_pixels, VARSIZE* inSz){
// This function is executed for each file in the images folder.
// After you load the content of the file you can perform whatever preprocessing you want

  int input_type;

  input_type= 1; //set the input type, 0 binary, 1 txt floating point

  switch (input_type){
    case 0:
      printf ("Loading bin file\n");
      /*load_fixed expects images stored in binary format, the data shape (CHW or HWC), the data size (16 bit or 8 bit per pixel) */
      /*and the representation (0 - 255; 0 - 1; -1 - 1) depends on the model and on the way it has been trained.*/
      load_fixed(image_path,784,image_pixels);// loads binary files
      break;
    case 1:
      printf ("Loading txt file\n");
      // loads txt files applying a normalization (the value depends on the dataset. Often it is the maximum value, for example 255.0) 
      load_float_txt(image_path,784,image_pixels,8, 1);
      break;
    default:
      printf ("Wrong input type!\n");
      exit(1);
      break;
  }
}


int resultsProcessing(DATA* results, int size){
//What do you want to do with the results of the CNN? Here is the place where you should put the classifier or the detection (see YOLO detection for example)
//The simplest classifier is a maximum search for the results which returns the index value of the maximum

 char *labels[10]={"digit 0", "digit 1", "digit 2", "digit 3", "digit 4", "digit 5", "digit 6", "digit 7", "digit 8", "digit 9"};

// TODO: check the size parameter
  int size_wa = 10;
  float* r= (float*) malloc (size_wa*sizeof(float));
  int*  c= (int*)  malloc (size_wa*sizeof(int));
  float* results_float= (float*) malloc (size_wa*sizeof(float));
  float sum=0.0;
  DATA max=0;
  for (int i =0;i<size_wa;i++){
      results_float[i] = FIXED2FLOAT(results[i],8);
    int n;
    if (results[i]>0)
      n=results[i];
    else
      n=-results[i];
    if (n>max){
      max=n;
    }  
  }
  for (int i =0;i<size_wa;i++)
    sum+=exp(results_float[i]);

  for (int i =0;i<size_wa;i++){
    r[i]=exp(results_float[i]) / sum;
    c[i]=i;
  }
  for (int i =0;i<size_wa;i++){
    for (int j =i;j<size_wa;j++){
      if (r[j]>r[i]){
        float t= r[j];
        r[j]=r[i];
        r[i]=t;
        int tc= c[j];
        c[j]=c[i];
        c[i]=tc;
      }
    }
  }
  int top0=0;
  float topval=results_float[0];
  for (int i =1;i<size_wa;i++){
    if (results_float[i]>topval){
      top0=i;
      topval=results_float[i];
    }  
  }
  printf("\n\n");
  for (int i =0;i<5;i++){
    printf("            TOP %d: [%d] %s   ",i, c[i], labels[c[i]]);
    for (int j =0;j<100*r[i];j++)
      if (j<40) printf("#");
    printf(" %0.1f%\n",r[i]*100);
  }  
  printf("\n\n\n\n\n");
  return top0;


}
