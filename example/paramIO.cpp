#include "paramIO.h"


RET load_float_txt(const char* filename, size_t arraysize, DATA* array, int qf, float norm) {
	
	FILE* fh = NULL;
	float temp;
	size_t read = 0;
	
	fh = fopen(filename, "r");

  if (fh == NULL){
    printf ("File %s doesn't exist\n", filename);
    exit(1);
    }
	
	for(int i=0;i<arraysize;i++){
        fscanf(fh,"%f",&temp);
        array[i]=FLOAT2FIXED(temp/norm, qf);
    }
	
	fclose(fh);

	return __OK__;
}


RET load_fixed(const char* filename, size_t arraysize, DATA* array) {
	FILE* fh = NULL;
	//unsigned int i = 0;
	size_t read = 0;
	
	fh = fopen(filename, "rb");

  if (fh == NULL){
    printf ("File %s doesn't exist\n", filename);
    exit(1);
    }
  else {
    printf ("File %s exists\n", filename);
  
  }
	read = fread(array, sizeof(DATA), arraysize, fh);

	if (read != arraysize)
	  printf("%s %s \ngiven: %d expected: %d", "fixed data input was not large enough: ", filename, read, arraysize);
	fclose(fh);
  
	return __OK__;
}


