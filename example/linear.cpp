#include "linear.h"



LINEAR linear_create(void) {
	LINEAR lin = (LINEAR) calloc(1, sizeof(struct Linear));
	return lin;
}

RET linear_init(LINEAR lin, NAME weightsFile, NAME biasFile, SIZE in_s,
		SIZE out_s) {
	SIZE weight_s = out_s * in_s;
	SIZE bias_s = out_s;
	RET res = __OK__;

	lin->weights = (DATA*) calloc(weight_s, sizeof(DATA));
	lin->bias = (DATA*) calloc(bias_s, sizeof(DATA));

	res = load_fixed(weightsFile, weight_s, lin->weights);


	res = load_fixed(biasFile, bias_s, lin->bias);


	lin->in_s = in_s;
	lin->out_s = out_s;
	return __OK__;
}

RET linear_forward_wrap(LINEAR lin){

return linear_forward(lin, lin->input, lin->output, lin->in_s, lin->out_s);

}

RET linear_forward(LINEAR lin, DATA* input, DATA* output, SIZE in_s,
		SIZE out_s) {



	linear_forward_sw_core(input, output, in_s, out_s, lin->weights,
			lin->bias, lin->qf);


	return __OK__;
}

RET linear_forward_sw_core(DATA* input, DATA* output, SIZE in_s, SIZE out_s,
		DATA* weights, DATA* bias, int qf) {

	// NOTE return W * x

	int hkern = 0;
	int wkern = 0;

	long long int mac = 0;





	DATA current = 0;

	/* foreach row in kernel */
//	#pragma omp parallel for private (hkern, wkern, mac, current)
	for (hkern = 0; hkern < out_s; hkern++) {

		mac = ((long long int)bias[hkern]) << qf;
		
		
		
		
		
		for (wkern = 0; wkern < in_s; wkern++) {
			current = input[wkern];
			mac += current * weights[hkern*in_s + wkern];
		}



			output[hkern] = (DATA)saturate(mac >> qf);

	}


	return __OK__;
}
static inline long long int saturate(long long int mac)
{

	if(mac > _MAX_) {
		printf("[WARNING] Saturation.mac: %lld -> %llx _MAX_: %d  _MIN_: %d  res: %d\n",  mac, mac, _MAX_, _MIN_, _MAX_);
		return _MAX_;	
	}
	
	if(mac < _MIN_){
		printf( "[WARNING] Saturation. mac: %lld -> %llx _MAX_: %d  _MIN_: %d  res: %d\n",  mac, mac, _MAX_, _MIN_, _MIN_);
		return _MIN_;
	}
	
	//printf("mac: %lld -> %llx _MAX_: %lld  _MIN_: %lld  res: %lld\n", mac, mac, _MAX_, _MIN_, mac);
    return mac;

}
RET linear_destroy(LINEAR lin) {
	free(lin->weights);
	free(lin->bias);
	free(lin);
	return __OK__;
}
