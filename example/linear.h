#ifndef LINEAR_H
#define LINEAR_H

#include "types.h"
#include "paramIO.h"

struct Linear {
	DATA* weights;
	DATA* bias;
	VARSIZE in_s;
	VARSIZE out_s;
	int qf;
	DATA* input;
	DATA* output;
    
	
};

typedef struct Linear* LINEAR;

RET linear_forward_sw_core(DATA* input, DATA* output, SIZE in_s, SIZE out_s,
		DATA* weights, DATA* bias, int qf);
LINEAR linear_create(void);

RET linear_forward_wrap(LINEAR lin);

RET linear_init(LINEAR lin, NAME weightsFile, NAME biasFile, SIZE in_s,
		SIZE out_s);

RET linear_forward(LINEAR lin, DATA* input, DATA* output, SIZE in_s,
		SIZE out_s);

RET linear_destroy(LINEAR lin);
static inline long long int saturate(long long int mac);
#endif
