#include "mnist_only_FC.h"
#include "types.h"
#include <string.h>

int main(){



DATA* image_pixels;
VARSIZE inSz[]={1,28,28};
DATA* results;

char wpath[100];
char ipath[100];


printf("DATASIZE [B]: %d\n", sizeof(DATA));
printf("MAXVALUE    : %d\n", _MAX_);
printf("MINVALUE    : %d\n", _MIN_);

// Defines the paths for parameters and inputs files
strcpy (wpath, "./weights");
strcpy (ipath, "./mnist_digits_all/6.txt");

image_pixels = (DATA*)malloc(sizeof(DATA)*1*28*28);
results      = (DATA*)malloc(sizeof(DATA)*10);



//Initializes the net, loads the weights and defines the layers
printf ("Initialization...\n");
cnnMainInit  (wpath);
printf ("Init done\n\n");

printf ("Loading image...\n");
//Loads the inputs images
getInputImage(ipath, image_pixels, inSz);
printf ("Image loaded\n\n");

printf ("Executing net...\n");
//Executes the net
cnnMain(image_pixels, &results);
printf ("Execution done\n\n");

//Post-processes the results of the net and returns the most likely class 
int topRes = resultsProcessing(results, 0);

return topRes;

}
