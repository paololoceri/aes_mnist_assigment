/******************************************************************************
 *                                                                            *
 *                   EOLAB @ DIEE - University of Cagliari                    *
 *                          Via Marengo, 2, 09123                             *
 *                       Cagliari - phone 070 675 5009                        *
 *                                                                            *
 *                 Gianfranco Deriu - gian.deriu@gmail.com                    *
 *                   Marco Carreras - marco.carreras@unica.it                 *
 *                     Paolo Meloni - paolo.meloni@unica.it                   *
 *                                                                            *
 * Project:     NEURAGHE - Accelerator for Convolutional neural network       *
 * File:        mnist_only_FC.h                                                  
 * Description: Auto generated project. Edit it as you like                   *
 *                                                                            *
 ******************************************************************************/
#ifndef _mnist_only_FC_
#define _mnist_only_FC_


#include "types.h"
#include "linear.h"
#include "relu.h"
#include "paramIO.h"
/*

your code here

*/




void cnnMainInit(VARNAME load_data_dir);
void cnnMain(DATA* image, DATA** results);
int  resultsProcessing(DATA* results, int size);


void getInputImage(VARNAME image_path, DATA* image_pixels, VARSIZE* inSz);

void getInputSize (VARSIZE* inSz);
void getOutputSize(VARSIZE* output_size);



#endif
