#ifndef RELU_H
#define RELU_H

#include "types.h"
#include "paramIO.h"

struct Relu {
	DATA* input;
	DATA* output;
	DATA alpha; // if alpha ==0 -> standard RELU, if alpha > 0 -> Leaky RELU
	
	VARSIZE size[3];
	
	int qf;
};

typedef struct Relu* RELU;

RELU relu_create(void);
RET relu_forward_wrap(RELU r);
RET relu_forward(DATA* input, DATA* output, SIZE size[3], DATA alpha, int qf);

#endif
